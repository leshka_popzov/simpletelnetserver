package telnet.shell;

import java.nio.channels.SocketChannel;


public interface basicCommand {
	
	public String getId();
	
	public void setId(String id);

	public String exec(String[] cmd, terminal term, SocketChannel channel);

}
