package telnet.shell;

import java.io.File;
import java.nio.channels.SocketChannel;

public class innerCommand extends abstractCommand {

	public innerCommand(String id){
		super(id);
	}
	
	public String exec(String[] cmd, terminal term, SocketChannel channel) {
	
		return null;//TODO
	}

}

//------------------------------------------------------------

class quitCommand extends innerCommand{

	public quitCommand(String id) {
		super(id);
	}
	
	public String exec(String[] cmd, terminal term, SocketChannel channel) {
		
		return abstractCommand.QUIT;
	}
	
}

//-----------------------------------------------

class cdCommand extends innerCommand{

	public cdCommand(String id) {
		super(id);
	}
	
	public String exec(String[] cmd, terminal term, SocketChannel channel) {
		
		if(cmd.length > 1 && new File(cmd[1]).exists()){
			term.setCurreantPath(cmd[1]);
		return abstractCommand.ALLRIGHT;
		}
		return abstractCommand.ERROR;
	}
	
}

//-------------------------------------------------
