package telnet.shell;

import java.nio.channels.SocketChannel;


public abstract class abstractCommand implements basicCommand {

	private String id;
	protected final static String ALLRIGHT = "ALLRIGHT";
	protected final static String ERROR = "ERROR";
	protected final static String QUIT = "QUIT";

	public abstractCommand(String id){
		setId(id);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public abstract String exec(String[] cmd, terminal term, SocketChannel channel);

}
