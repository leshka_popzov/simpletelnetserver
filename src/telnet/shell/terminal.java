package telnet.shell;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.StringTokenizer;
import java.util.logging.Level;

import telnet.*;

public class terminal {
	private static String[] OS = null;
	private SocketChannel m_channel;
	//private HashSet<Byte> m_states = new HashSet<Byte>();
	private manager m_manager = new manager(this);

	static{

		if( System.getProperty("os.name").equals("Windows NT")){
			OS = new String[2];
			OS[0] = "cmd.exe";
			OS[1] = "/C";
		}
		else if( System.getProperty("os.name").equals("Windows 95")){
			OS = new String[2];
			OS[0] = "command.com";
			OS[1] = "/C";
		}
	}

	private String m_curreantPath = System.getProperty("user.dir");
	private commandFactory m_CF = new commandFactory();

	public terminal( SocketChannel channel){
		m_channel = channel;
//		m_states.add(manager.GO_AHEAD);
	//	m_states.add(manager.ECHO);
		synchronization();
	}

	public String executor(String[] cmd){
		return m_CF.find(cmd[0]).exec(cmd, this, m_channel);
	}

	public String exec(ByteBuffer command){
		if(command.get(0) != -1){
			String buffer = Server.CS.decode(command).toString(); 
			Server.logger.log(Level.INFO,"getting new command: " + buffer);
			StringTokenizer tokenazer = new StringTokenizer(buffer);
			if(OS != null){
				String[] cmd = new String[OS.length+tokenazer.countTokens()];
				int i = 0;
				for(String el: OS)
					cmd[i++] = el;
						while(tokenazer.hasMoreTokens())
							cmd[i++] = tokenazer.nextToken();

						return executor(cmd);
			}
			else
				return executor(buffer.trim().split("\\s"));
		}
		else{
			if(m_manager.analze(command.array()) == null)//TODO смотри метод в классе manager
				return abstractCommand.ALLRIGHT;
			else
				return abstractCommand.ERROR;
		}
	}

	public String getCurreantPath() {
		return m_curreantPath;
	}

	public void setCurreantPath(String curreantPath) {
		m_curreantPath = curreantPath;
	}

	public void synchronization(){
		m_manager.synchronze(m_channel);
	}
}

