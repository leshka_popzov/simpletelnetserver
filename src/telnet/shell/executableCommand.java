package telnet.shell;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.logging.Level;

import telnet.*;

class processReader extends Thread{
	private final InputStream m_stream;
	private final SocketChannel m_channel;
	boolean quit = false;

	processReader(final InputStream stream, SocketChannel channel) {
		m_stream = new BufferedInputStream(stream); // поток из которого читаем
		m_channel= channel;  //сетевой канал в который пишем
	}

	public void run() {
		try {
			Integer c = null;
			byte[] buffer = new byte[1];
			while(!quit){
				while ((c = m_stream.read()) != -1){
					buffer[0] =  c.byteValue();
					m_channel.write(ByteBuffer.wrap(buffer));
				}
			}
		} catch (IOException e) {
			Server.logger.log(Level.WARNING, "Error while reading process output");
		}

	}
	public void quit(){
		quit = true;
	}
}
//---------------------------------------------------------------
class processWriter extends Thread{
	private final OutputStream m_stream; 	//поток в который пишем
	private final SocketChannel m_channel;
	private boolean quit = false;//сетевой канал из которого читаем

	processWriter(final OutputStream stream, SocketChannel channel) {
		m_stream =  new BufferedOutputStream(stream);
		m_channel= channel;
	}

	public void run() {
		try {
			ByteBuffer buffer = ByteBuffer.allocate(64);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(m_stream));
			String s = null;
			while(!quit){
				if(m_channel.read(buffer)>0){
					s = new String(((ByteBuffer) buffer.flip()).array());
					//System.out.println(s);
					writer.write(s);
					writer.flush();
				}
			}

		} catch (Exception e) {
			Server.logger.log(Level.WARNING, "Error while writing in process");
		}

	}

	public void quit(){
		quit= true;
	}

}
//---------------------------------------------------------------
public class executableCommand extends abstractCommand{

	private Process m_process;
	private ProcessBuilder m_builder;
	protected ArrayList<String> m_message = new ArrayList<String>();
	private processReader m_errorReader = null, m_reader = null;
	private processWriter m_writer = null;

	public executableCommand(String id){
		super(id);

	}

	public String exec(String[] cmd, terminal term, SocketChannel channel){
		m_builder = new ProcessBuilder(cmd).directory(new File(term.getCurreantPath()));
		try{
			m_process = m_builder.start();
			m_errorReader = new processReader(m_process.getErrorStream(),channel);
			m_reader = new processReader(m_process.getInputStream(),channel);
			m_writer = new processWriter(m_process.getOutputStream(), channel);
		}catch(IOException e){
			Server.logger.log(Level.WARNING,
					"Cann't execute " +  cmd[0]);
			return abstractCommand.ERROR;
		}
		m_errorReader.start();
		m_reader.start();
		m_writer.start();
		try {
			m_process.waitFor();
			m_writer.quit();
			m_errorReader.quit();
			m_reader.quit();
			return abstractCommand.ALLRIGHT;

		} catch (InterruptedException e){
			e.printStackTrace();
		}
		return abstractCommand.ERROR; 
	}

	public void processBreak(){
		m_process.destroy();
		m_writer.quit();
		m_errorReader.quit();
		m_reader.quit();
	}

}
///-----------------------------------------------------------------------------
class defaultCommand extends executableCommand{

	public defaultCommand(String id) {
		super(id);
	}

	public String exec(String[] cmd, terminal term, SocketChannel channel){
		return super.exec(cmd, term, channel);
	}

}
//---------------------------------------------------------------------
class oneByteCommand extends executableCommand{

	public oneByteCommand(String id) {
		super(id);
	}

	public String exec(String[] cmd, terminal term, SocketChannel channel){
		manager.WILL(manager.ECHO,channel);
		manager.WILL(manager.GO_AHEAD,channel);
		String rez = super.exec(cmd, term, channel);
		manager.WONT(manager.ECHO,channel);
		manager.WONT(manager.GO_AHEAD,channel);
		return rez;
	}
}
//-------------------------------------------------------------------
class badCommand extends executableCommand{

	public badCommand(String id) {
		super(id);
	}

	public String exec(String[] cmd, terminal term, SocketChannel channel){
		try {
			channel.write(ByteBuffer.wrap(new String("Server cann't work with this application!We are sorry\r\n").getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
			return abstractCommand.ERROR;
		}

		return abstractCommand.ALLRIGHT;
	}
}
//----------------------------------------------------------------------





