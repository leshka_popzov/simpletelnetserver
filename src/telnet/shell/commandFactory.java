package telnet.shell;

import java.util.HashMap;


public class commandFactory {
	private HashMap<String, basicCommand> m_commands = new HashMap<String, basicCommand>();
	private basicCommand m_cmd;
	static String DEFAULT = "default";
	public commandFactory() {
		m_cmd = new defaultCommand(DEFAULT);//TODO
		m_commands.put(m_cmd.getId(), m_cmd);
		m_cmd = new quitCommand("quit");
		m_commands.put(m_cmd.getId(), m_cmd);
		m_cmd = new cdCommand("cd");
		m_commands.put(m_cmd.getId(), m_cmd);
		m_cmd = new oneByteCommand("htop");
		m_commands.put(m_cmd.getId(), m_cmd);
		m_cmd = new oneByteCommand("vim");
		m_commands.put(m_cmd.getId(), m_cmd);
		m_cmd = new badCommand("mc");
		m_commands.put(m_cmd.getId(), m_cmd);
	}

	public basicCommand find(String key){
		if(m_commands.get(key)!= null)
			return  m_commands.get(key);
				
		return m_commands.get(DEFAULT);
		//TODO проверка на наличи управляющего символа
	}
}
