package telnet.network;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.logging.Level;

import telnet.shell.*;
import telnet.*;

public class connectionProcess implements Runnable{
	private SocketChannel m_channel;
	private Selector m_selector;
	private boolean quit = false;
	private terminal m_terminal;

	public connectionProcess(SocketChannel ch){
		m_channel = ch;
		try{
			m_selector = Selector.open();
			m_channel.register(m_selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
			m_terminal = new terminal(m_channel);
			Server.logger.log(Level.INFO, "connection establish on " + m_channel);
		}catch(Exception e){
			Server.logger.log(Level.WARNING,
					"Connection from" + ch + "cann't be processed");
			quit = true;
			channel_close();
		}
	}

	public void channel_close(){ 
		try {
			m_channel.close();
		}
		catch (IOException ex){
			Server.logger.log(Level.WARNING,
					"error while closing " + m_channel);
		}
	}

	public void clean_buffer(ByteBuffer buffer){//TODO
		buffer.clear();
		byte zero = 0;
		for(int i = 0;i < buffer.limit();i++)
			buffer.put(zero);
		buffer.clear();
	}

	public void run(){
		try{
		m_channel.write(ByteBuffer.wrap("Simple telnet server is ready\r\n".getBytes()));
		}catch(IOException e){
			Server.logger.log(Level.WARNING,
					"error while procesing channel: " + m_channel.hashCode());
			quit = true;
		}
		if(!quit){
			ByteBuffer buffer = ByteBuffer.allocate(64);
			String rez = null;
			while(!quit){
				try{
					m_selector.select();
					Iterator<SelectionKey> it = m_selector.selectedKeys().iterator();
					while (it.hasNext()){
						SelectionKey key = it.next();
						it.remove();
						if (key.isReadable() && m_channel.read(buffer) > 0){
							rez = m_terminal.exec((ByteBuffer) buffer.flip());
							if(!rez.equals("ALLRIGHT")){
								if(rez.equals("QUIT")){
									quit = true;
									m_channel.write(ByteBuffer.wrap("goodbuy\r\n".getBytes()));
								}
								if(rez.equals("ERROR")){
									m_channel.write(ByteBuffer.wrap("bad command\r\n".getBytes()));
								}
							}
							clean_buffer(buffer);	
						}
					}
				}catch(IOException e){
					Server.logger.log(Level.WARNING,
							"error while procesing channel: " + m_channel.hashCode());
					quit = true;
				}
			}
			channel_close();
		}
	}
}






