package telnet.network;

import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.logging.Level;

import telnet.*;

public class connectionsManager {
	private static int PORT = 23;
	private static int POOL_CAPACITY = 25;
	private ServerSocketChannel serverChannel;
	private Selector selector;
	private static ThreadPool pool;

	public connectionsManager() {
		this(PORT);
	}

	public connectionsManager(int port) {
		PORT = port;
		try{
			serverChannel = ServerSocketChannel.open();
			selector = Selector.open();
			serverChannel.configureBlocking(false);
			pool = new ThreadPool(POOL_CAPACITY);
			manage();
		}catch(Exception e){
			Server.logger.log(Level.SEVERE,
					"Server init error");
		}

	}

	public void manage()
	{
		try{
			serverChannel.socket().bind(new InetSocketAddress(PORT));
		}catch(Exception e){
			Server.logger.log(Level.SEVERE,
					"PORT = " + PORT + " cann't be bind");
			System.exit(e.hashCode());
		}

		try {
			serverChannel.register(selector, SelectionKey.OP_ACCEPT);
			Server.logger.log(Level.INFO,
					"PORT = " + PORT + " Starting telnet server on port: " + PORT);
			while (true) {
				selector.select();
				Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
				while (iterator.hasNext()) {
					SelectionKey skey =  iterator.next();
					iterator.remove();
					if (skey.isAcceptable()) {
						SocketChannel channel = serverChannel.accept();
						//!!!!!!!!!!!!System.out.println("connection from:"+ channel.socket());
						channel.configureBlocking(false);
						pool.addTask(new connectionProcess(channel));
					}
				}
			}
		}catch(Exception e){
			Server.logger.log(Level.SEVERE,
					"Error in connections managment");
			System.exit(e.hashCode());
		}
	}

}

