package telnet.network;

import java.util.*;
import java.util.logging.*;

import telnet.*;

class Worker extends Thread {
	
	private String workerId;
	private Runnable task;
	private ThreadPool threadPool;


	public Worker(String id, ThreadPool pool) {
		workerId = id;
		threadPool = pool;
		start();
	}

	public void setTask(Runnable t) {
		task = t;
		synchronized (this) {
			notify();
		}
	}

	public void run() {
		try {
			while (true) {
				synchronized (this) {
					if (task != null) {
						try {
							task.run(); 
						}
						catch (Exception e) {
							Server.logger.log(Level.SEVERE,
									"Exception in source Runnable task", e);
						}

						threadPool.putWorker(this);
					}
					wait();
				}
			}

		}
		catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public String toString() {
		return "Worker : " + workerId;
	}
}


public class ThreadPool extends Thread {
	private static final int DEFAULT_NUM_WORKERS = 10;
	private LinkedList<Worker> workerPool = new LinkedList<Worker>();
	private LinkedList<Runnable> taskList = new LinkedList<Runnable>();

	public ThreadPool() {
		this(DEFAULT_NUM_WORKERS);
	}

	public ThreadPool(int numOfWorkers) {
		for (int i = 0; i < numOfWorkers; i++)
			workerPool.add(new Worker("" + i, this));
		start();
	}

	public void run() {
		try {
			while (true) {

				if (taskList.isEmpty()) {
					synchronized (taskList) {
						taskList.wait();
					}
				}
				else if (workerPool.isEmpty()) {
					synchronized (workerPool) {
						workerPool.wait();
					}
				}

				getWorker().setTask(taskList.removeFirst());
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void addTask(Runnable task) {
		taskList.addLast(task);
		synchronized (taskList) {
			taskList.notify();
		}
	}

	public void putWorker(Worker worker) {
		workerPool.addLast(worker);
		synchronized (workerPool) {
			workerPool.notify();
		}
	}

	private Worker getWorker() {
		return (Worker) workerPool.removeLast();
	}

} 

