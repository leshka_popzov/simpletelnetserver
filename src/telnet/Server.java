package telnet;

import java.nio.charset.Charset;
import java.util.logging.*;

import telnet.network.*;

public class Server {
	private static connectionsManager m_connectionsManager;
	public static final int PORT = 23;
	public static final Charset CS = Charset.defaultCharset();
	public static Logger logger;

	public static void initLogger(){
		if(logger == null){
			logger = Logger.getLogger("System");
			try{
				logger.addHandler(new ConsoleHandler());
				logger.addHandler(new FileHandler("telnetServer.log"));
				logger.setUseParentHandlers(false);
			}catch(Exception e){
				System.out.println("logger init error");
			}
		}
	}

	public  static void initConnectionsManager(int port){
		if(m_connectionsManager == null)
			m_connectionsManager = new connectionsManager(port);
	}
	public static void initConnectionsManager(){
		initConnectionsManager(PORT);
	}

	public static void main(String[] args) {
		initLogger();
		initConnectionsManager(PORT);
	}
}


