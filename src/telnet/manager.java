package telnet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;

import telnet.shell.terminal;

public class manager {
	public static final byte IAC = (byte) 255; 
	public static final byte WILL = (byte) 251; 
	public static final byte WONT = (byte) 252; 
	public static final byte DO = (byte) 253; 
	public static final byte DONT = (byte) 254; 
	public static final byte ECHO  = (byte) 1; 
	public static final byte GO_AHEAD  = (byte) 3; 
	protected final static String ALLRIGHT = "ALLRIGHT";
	protected final static String ERROR = "ERROR";
	private static HashMap<String, Byte> state = new HashMap<String, Byte>();
	//private terminal m_terminal;
	static {
		state.put("echo", ECHO);
		state.put("GO AHEAD", GO_AHEAD);
	}

	public manager(terminal term){
		//m_terminal = term;
	}

	public static String WILL(Byte b,SocketChannel channel){
		byte[] buffer = {IAC, WILL, b};
		return write(buffer,channel);
	}

	public static String WONT(Byte b,SocketChannel channel){

		byte[] buffer = {IAC, WONT, b};
		return write(buffer,channel);

	}

	public static String WILL(String str,SocketChannel channel){
		Byte b = state.get(str);
		if(b == null)
			b = Byte.parseByte(str);
		byte[] buffer = {IAC, WILL, b.byteValue()};
		return write(buffer,channel);
	}

	public static String WONT(String str,SocketChannel channel){
		Byte b = state.get(str);
		if(b == null)
			b = Byte.parseByte(str);
		byte[] buffer = {IAC, WONT, b.byteValue()};
		return write(buffer,channel);

	}

	private static String write(byte[] buffer, SocketChannel channel){
		try {
			channel.write(ByteBuffer.wrap(buffer));
		} catch (IOException e) {
			e.printStackTrace();
			return ERROR;
		}
		return ALLRIGHT;
	}

	public String synchronze(SocketChannel channel){
		//TODO предусмотреть логику синхронизации- сейчас тупо влоб!!!!
		WONT(ECHO,channel);
		WONT(GO_AHEAD,channel);
		
		return ALLRIGHT;

	}
	
	public byte[] analze(byte[] cmd){
		//TODO обработчик входящих управляющих команд
		return null;
	}
	
}
